/**
 * Copyright (c) 2016 Abelardo López Lagunas
 *
 * @file    UserDefined.h
 *
 * @author  Abelardo López Lagunas
 *
 * @date    Fri 13 May 2016 10:07 DST
 *
 * @brief   Declares all the user defined functions for handling the
 *          specific user-defined data structure that is pointed to
 *          by the doubly linked list node.
 *
 * References:
 *          Code loosely based on my Generic Singly linked list algorithm.
 *
 * Revision history:
 *          Fri 13 May 2016 10:07 DST -- File created
 *
 * @warning If there is not enough memory to create a node or the hash
 *          management fails the related function indicates failure.
 *          If the DEBUG compiler flag is set then the program will terminate
 *          but more information is displayed indicating the source of error.
 *
 * @note    The code is meant to show how to implement hash tables using GLib
 *
 */

#include <glib.h>
#include "types.h"

/**
 * @struct item
 *
 * @brief This is the basic user-defined element of the hash table
 *
 * The user-defined data structure is a @c string and a @c pointer to the
 * the symbol table entry.
 *
 */
typedef struct item_{
   char * key;                           /**< Hash table key is a string */
   void * tableEntry;           /**< Pointer to a generic data structure */
}item;

/**
 * @typedef item_p
 *
 * @brief declare a pointer to the @c item @c structure
 */
typedef struct item_ *item_p;        /**< Declaration of ptr to an entry */

/**
 * @union val
 *
 * @brief Defines the 32-bit value of a symbol table element.
 *
 * The @c val union defines the possible values for the elements in the
 * symbol table.
 *
 */
union val {            /* Note that both values are 32-bits in length */
   int     i_value;                   /**< Interpret it as an integer */
   float   r_value;                      /**< Interpret it as a float */
};

/**
 * @struct tableEntry
 *
 * @brief This is the user-defined symbol table entry.
 *
 * @c TableEntry is the user-defined structure that describes a symbol table
 * entry. Each entry has the following fields:
 *
 * @c name_p is a string holding the name of the variable. This may be
 *    different from the hash key (the key is the variable name plus the
 *    value of the current context).
 *
 * @c type indicates if the variable is integer or float.
 *
 * @c scope is an integer indicating the symbol table entry scope.
 *
 * @c lineNumber is the line number where the variable was defined.
 *
 * @c value is a union of all possible values (integer/float). Not space
 *    efficient if smaller types are allowed.
 *
 */
typedef struct tableEntry_{
   char           * name_p;            /**< The name is just the string */
   enum myTypes     type;                          /**< Identifier type */
   union val        value;       /**< Value of the symbol table element */

   GPtrArray * list_t;	/**< Quad list of true */
   GPtrArray * list_f;	/**< Quad list of false */
   GPtrArray * list_nxt;	/**< Quad list next */

}tableEntry;

/**
 * @typedef entry_p
 *
 * @brief declare a pointer to the @c tableEntry @c structure
 */
typedef struct tableEntry_ * entry_p; /**< Declaration of ptr to an entry */

union response{
	int address;
	entry_p rpt;
};

typedef struct quad_{
	char * op;
	union response resp;
	entry_p r1;
	entry_p r2;
}quad;

typedef struct quad_ * quadruple;

typedef char * string;/**< Declaration for strings*/


int PrintItem (entry_p theEntry_p);

void SupportPrint (gpointer key_p, gpointer value_p, gpointer user_p);

void UpdateData(char * s, enum myTypes type ,GHashTable * theTable_pp);

int PrintTable (GHashTable * theTable_p);

entry_p NewItem (enum myTypes type,GHashTable * theTable_pp,int i,union val v);

quadruple NewQuad (char * quadName_p, union response r, entry_p p1, entry_p p2);

void printQuad (quadruple q);

void printQuadAddress (quadruple q);

void backpatch(GPtrArray * actions,GPtrArray * list, int quad);

GPtrArray * merge(GPtrArray * list1,GPtrArray * list2);

GPtrArray * copy(GPtrArray * list);

GPtrArray * newList(int q);


/**
 *
 * @brief De-allocates memory assigned to user-defined data structure.
 *
 * @b FreeItem will de-allocate the @c string inside the user-defined data
 * structure @c tableEntry.
 *
 * @param  theEntry_p is a pointer to the user-defined data structure.
 * @return @c EXIT_SUCCESS if the item was de-allocated with no
 *         problems, otherwise return @c EXIT_FAILURE.
 *
 * @code
 *  FreeItem(theEntry_p);
 * @endcode
 *
 * @warning This function must be passed as a parameter when calling
 *          DestroyTable() since it will call it to de-allocate the
 *          user-defined structure.
 */
void FreeItem (entry_p theEntry_p);

/**
 *
 * @brief De-allocates memory assigned to each user-defined data structure in
 *        the hash table.
 *
 * @b DestroyTable will de-allocate the user-defined data structure. It also
 *    deallocates memory for the hash table.
 *
 * @param  theTable_p is a pointer to the hast table.
 * @return @c EXIT_SUCCESS if the list was de-allocated with no problems,
 *         otherwise return @c EXIT_FAILURE
 *
 * @code
 *  DestroyList(theList_p);
 * @endcode
 *
 * @see FreeItem()
 *
 */
/***********************
   Search in the hash table for a declarated variables
**********************
*/
entry_p searchVar(char * s, GHashTable * theTable_pp);


/******
	insertVar() -> Function to insert a new node with the name of the variable (char * s)
						enum myTypes -> type of the variable ('integer' or 'real')
						GHashTable -> the hash table to insert the symbol
*****/
void insertVar(enum myTypes type , char * s,GHashTable * theTable_pp);

/***********
	DestroyTable() -> the function to destroy the hash table
**********/
int DestroyTable (GHashTable * theTable_p);
/***********
   void execute_cycle(GPtrArray * garray)
   this function execute code from the QUADS
*************/
GHashTable* execute_cycle(GPtrArray * garray,GHashTable * theTable_p);

void printQuadTable(GPtrArray * garray);




