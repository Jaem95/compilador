
# Delete the old executables
rm Analizer

# Compile, -Wall option shows all warnings
bison -v SintacticAnalizer.y
flex LexicAnalizer.l
gcc SintacticAnalizer.tab.c UserDefined.c  `pkg-config --cflags --libs glib-2.0` -o Analizer -ll
