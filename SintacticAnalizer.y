//
//  main.c
//  lex_syn_an
//
//  Created by Jose Luis Santillan on 05/02/17.
//  Copyright © 2017 Jose Luis Santillan. All rights reserved.
//

%{
    
    /**********************
     * Declaraciones en C *
     **********************/
    
    
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "UserDefined.h"
    #include <glib.h>   
    #include <assert.h>           

    extern int yylex(void);
    extern char *yytext;
    extern int linea;
    int error2=0;
    int error3=0;
    extern FILE *yyin;
    
    int number=0;
    int semantic_error =0;

    //Función de error
    void yyerror(string input);
    void yyerrorL(int line);

    //Funcion de busqueda
    //void symlook(char * s, int type);
   

   // Declaración de la tabla de hash
   extern GHashTable * theTable_p; 

   //Quad control
   int quadC = 0;
   int tempC = 0;

    GPtrArray * garray;

    //int correct=0, lineno=1;

%}


/*************************
 Declaraciones de Bison *
 *************************/
//%start program;

%union{
    entry_p entry;
    char * string;
    int integerValue;
    float floatValue;
}



%token NAME 
%token IF 
%token THEN 
%token ELSE 
%token WHILE 
%token DO 
%token ASSIGN 
%token READ 
%token LPAREN 
%token RPAREN 
%token SEMICOLON 
%token WRITE 
%token LBRACKET 
%token RBRACKET 
%token LT 
%token MR 
%token EQ 
%token PLUS 
%token MINUS 
%token TIMES 
%token DIV 
%token <string>ID 
%token INTEGER 
%token FLOAT 


%token <integerValue>INT_NUM 
%token <floatValue>FLOAT_NUM
%error-verbose

%type <entry> exp simple_exp term factor variable n stmt stmt_seq  block
%type <integerValue> type 
%type <integerValue> m 

%%
/***********************
 * Reglas Gramaticales *
 ***********************/

program: var_dec stmt_seq{
				
			 }	
;

var_dec: var_dec single_dec
|   epsilon
;
single_dec: type ID SEMICOLON {
                                if(searchVar($2,theTable_p)==NULL){/*Check if the variable is already declared*/
                                		//insert varible in the hash table
                                   insertVar($1,$2,theTable_p); 
                                   //number of variables declared
                                   number++;
                                }else{
                                	//print error when a variable is already declared
					yyerror("Double declaration");
					//print the line where error was found
					yyerrorL(linea);
                                }
                              }
;

type:INTEGER{
                $$=integer;
            }
    |FLOAT{
                $$=real;
            }
;

stmt_seq: stmt_seq stmt m{
				//printf("M values%d\n",$3);
				//printf("%d",$2->list_nxt->len);
				//$$->list_nxt = copy($2->list_nxt);
				//backpatch(garray,$2->list_nxt, $3);
		       }
|   epsilon{

	   }
;

stmt: IF exp THEN m stmt {

			$$=malloc(sizeof(entry_p));

			backpatch(garray,$2->list_t,$4);
			$$->list_nxt = merge($2->list_f,$5->list_nxt);
		      }
|	IF exp THEN m stmt n m stmt {
					$$=malloc(sizeof(entry_p));

					backpatch(garray,$2->list_t,$4);
					backpatch(garray,$2->list_f,$7);

					$$->list_nxt = merge($5->list_nxt,merge($6->list_nxt,$8->list_nxt));
				  }
|	WHILE m exp DO m stmt {
				$$=malloc(sizeof(entry_p));
				long x;
				//x = (long)g_ptr_array_index($3->list_t,0);
				//printf(" list true: %ld\n",x); 

				//x = (long)g_ptr_array_index($3->list_f,0);
				//printf(" list false: %ld\n",x); 

				//printf("M values%d\n",$5);

				backpatch(garray,$3->list_t,$5);

				$$->list_nxt = $3->list_f;

				quadruple temp;
				union response resp;
				resp.address = $2;
				temp = NewQuad("goto",resp,NULL,NULL);
				printQuadAddress(temp);
				g_ptr_array_add (garray, (gpointer) temp);
				quadC++;


			     }
|	variable ASSIGN exp SEMICOLON      {
						if($1->type == integer && $3->type== integer)
						{
							//In case both have the same types we create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = $1;
							temp = NewQuad("ASSIGN",resp,$3,NULL);

							printQuad(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;
	
						}
						else
						{
							if($1->type == real && $3->type== integer)
							{
								//we check if the types are not the same, if the result variable 
								// is integer but the expression use float the cohersion produce
								// lost of precision.
								yyerror("*****Error: *****\nThe operation result := is float");
									//print warning
								yyerror("Error: cohersion will produce lost of precision\n**************");
								yyerrorL(linea);

								//create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = $1;
								temp = NewQuad("ASSIGN",resp,$3,NULL);

								printQuad(temp);

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;
								}
							else
							{
								if($1->type == integer && $3->type== real){
									//we check if the types are not the same, if the result variable 
									// is integer but the expression use float the cohersion produce
									// lost of precision.
									yyerror("*****Error: *****\nThe operation result := is integer");
										//print warning
									yyerror("Error: cohersion will produce lost of precision\n**************");
									yyerrorL(linea);

									//create the corresponding quad
									quadruple temp;
									union response resp;
									resp.rpt = $1;
									temp = NewQuad("ASSIGN",resp,$3,NULL);

									printQuad(temp);

									g_ptr_array_add (garray, (gpointer) temp);
									quadC++;
									}
								else
								{
									if($1->type == real && $3->type== real)
									{
							
										//In case both have the same types we create the corresponding quad
										quadruple temp;
										union response resp;
										resp.rpt = $1;
										temp = NewQuad("ASSIGN",resp,$3,NULL);

										printQuad(temp);

										g_ptr_array_add (garray, (gpointer) temp);
										quadC++;
									}
									else
									{
										yyerror("Error: in different types\n**************");
										yyerrorL(linea);
									}
								}
							}
						}
					}

            
|	READ LPAREN variable RPAREN SEMICOLON{
						//create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = $3;
						temp = NewQuad("READ",resp,NULL,NULL);

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;
					     }
|	WRITE LPAREN exp RPAREN SEMICOLON{
						//create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = $3;
						temp = NewQuad("WITE",resp,NULL,NULL);

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);	
						quadC++;
					 }
|	block{
		$$=malloc(sizeof(entry_p));
		$$ = $1;
	     }
;

m: {
	$$ = quadC + 1;
  }
;

n: ELSE{
	$$=malloc(sizeof(entry_p));
	$$->list_nxt = newList(quadC);
      }
;


block: LBRACKET stmt_seq RBRACKET     {
					$$=malloc(sizeof(entry_p));
					$$ = $2;
			   	      }
        
;

exp: simple_exp LT simple_exp {
						$$=malloc(sizeof(entry_p));

						//we check the types of the variables are the same
						if($1->type != $3->type){
							yyerror("*****Error: *****\nThe operation < has diferent types\n**************");
							yyerrorL(linea);
						}
						else{
							//In case both have the same types we create the corresponding quad
							quadruple temp;
							union response resp;
							resp.address = 3;
							temp = NewQuad("LT",resp,$1,$3);

							printQuadAddress(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							//True list for the production
							$$->list_t = newList(quadC);

							//False List for the production
							$$->list_f = newList(quadC+1);

							resp.address = 0;
							temp = NewQuad("goto",resp,NULL,NULL);
							printQuadAddress(temp);
							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

						}
			      }     
| simple_exp MR simple_exp {
						$$=malloc(sizeof(entry_p));

						//we check the types of the variables are the same
						if($1->type != $3->type){
							yyerror("*****Error: *****\nThe operation > has diferent types\n**************");
							yyerrorL(linea);
						}
						else{
							//In case both have the same types we create the corresponding quad
							quadruple temp;
							union response resp;
							resp.address = 0;
							temp = NewQuad("MR",resp,$1,$3);
							
							printQuadAddress(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							//True list for the production
							$$->list_t = newList(quadC);

							//False List for the production
							$$->list_f = newList(quadC+1);

							resp.address = 0;
							temp = NewQuad("goto",resp,NULL,NULL);
							printQuadAddress(temp);
							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;
						}

			   }
| simple_exp EQ simple_exp {
				$$=malloc(sizeof(entry_p));

				//we check the types of the variables are the same
				if($1->type != $3->type){
					yyerror("*****Error: *****\nThe operation = has diferent types\n**************");
					yyerrorL(linea);
				}
				else{
					//In case both have the same types we create the corresponding quad
					quadruple temp;
					union response resp;
					resp.address = 0;
					temp = NewQuad("EQ",resp,$1,$3);

					printQuadAddress(temp);

					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;

					//True list for the production
					$$->list_t = newList(quadC);

					//False List for the production
					$$->list_f = newList(quadC+1);

					resp.address = 0;
					temp = NewQuad("goto",resp,NULL,NULL);
					printQuadAddress(temp);
					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;
				}
			   }                  
| simple_exp               {
			   	$$ = $1;
			   }                    
;

simple_exp: simple_exp PLUS term    {
					if($1->type == integer && $3->type== integer)
					{

						//Create the a temporal 
						entry_p actual; 
						union val value;
	    					value.i_value = 0;
						actual = NewItem(integer,theTable_p,tempC,value);
						tempC++;

						//We create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = actual;
						temp = NewQuad("PLUS",resp,$1,$3);

						printQuad(temp);	

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;

						$$ = actual;
					}
					else
					{
						if($1->type == real && $3->type== integer)
						{
							//we check if the types are not the same, if the result variable 
							// is integer but the expression use float the cohersion produce
							// lost of precision.
							yyerror("+++++++++\nThe operation + has diferent types");
								//print warning
							yyerror("Error: cohersion will produce lost of precision\n**************");
							yyerrorL(linea);

							//Create the a temporal 
							entry_p actual; 
							union val value;
	    						value.r_value = 0.0;
							actual = NewItem(real,theTable_p,tempC,value);
							tempC++;

							//We create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = actual;
							temp = NewQuad("PLUS",resp,$1,$3);

							printQuad(temp);	

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;
							$$ = actual;
						}
						else
						{
							if($1->type == integer && $3->type== real){
								//we check if the types are not the same, if the result variable 
								// is integer but the expression use float the cohersion produce
								// lost of precision.
								yyerror("+++++++++\nThe operation + has diferent types");
									//print warning
								yyerror("Error: cohersion will produce lost of precision\n**************");
								yyerrorL(linea);

								//Create the a temporal 
								entry_p actual; 
								union val value;
	    							value.r_value = 0.0;
								actual = NewItem(real,theTable_p,tempC,value);
								tempC++;

								//We create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = actual;
								temp = NewQuad("PLUS",resp,$1,$3);

								printQuad(temp);	

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;

								$$ = actual;
						}
							else
							{
								if($1->type == real && $3->type== real)
								{
									//Create the a temporal 
									entry_p actual; 
									union val value;
	    								value.r_value = 0.0;
									actual = NewItem(real,theTable_p,tempC,value);
									tempC++;

									//We create the corresponding quad
									quadruple temp;
									union response resp;
									resp.rpt = actual;
									temp = NewQuad("PLUS",resp,$1,$3);

									printQuad(temp);	

									g_ptr_array_add (garray, (gpointer) temp);
									quadC++;

									$$ = actual;
								}
								else
								{
									yyerror("Error: in different types\n**************");
									yyerrorL(linea);
								}
							}
						}
					}
			            }  
         
| simple_exp MINUS term {
			   if($1->type == integer && $3->type== integer)
				{
					//Create the a temporal 
					entry_p actual; 
					union val value;
	    				value.i_value = 0;
					actual = NewItem(integer,theTable_p,tempC,value);
					tempC++;

					//We create the corresponding quad
					quadruple temp;
					union response resp;
					resp.rpt = actual;
					temp = NewQuad("MINUS",resp,$1,$3);

					printQuad(temp);
					
					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;

					$$ = actual;
				}
				else
				{
					if($1->type == real && $3->type== integer)
					{
						//we check if the types are not the same, if the result variable 
						// is integer but the expression use float the cohersion produce
						// lost of precision.
						yyerror("+++++++++\nThe operation - has diferent types");
							//print warning
						yyerror("Error: cohersion will produce lost of precision\n**************");
						yyerrorL(linea);

						//Create the a temporal 
						entry_p actual; 
						union val value;
	    					value.r_value = 0.0;
						actual = NewItem(real,theTable_p,tempC,value);
						tempC++;

						//We create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = actual;
						temp = NewQuad("MINUS",resp,$1,$3);

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;

						$$ = actual;
					}
					else
					{
						if($1->type == integer && $3->type== real){
							//we check if the types are not the same, if the result variable 
							// is integer but the expression use float the cohersion produce
							// lost of precision.
							yyerror("+++++++++\nThe operation - has diferent types");
								//print warning
							yyerror("Error: cohersion will produce lost of precision\n**************");
							yyerrorL(linea);
							
							//Create the a temporal 
							entry_p actual; 
							union val value;
	    						value.r_value = 0.0;
							actual = NewItem(real,theTable_p,tempC,value);
							tempC++;

							//We create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = actual;
							temp = NewQuad("MINUS",resp,$1,$3);

							printQuad(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							$$ = actual;
					}
						else
						{
							if($1->type == real && $3->type== real)
							{
								//Create the a temporal 
								entry_p actual; 
								union val value;
	    							value.r_value = 0.0;
								actual = NewItem(real,theTable_p,tempC,value);
								tempC++;

								//We create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = actual;
								temp = NewQuad("MINUS",resp,$1,$3);

								printQuad(temp);

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;

								$$ = actual;
							}
							else
							{
								yyerror("Error: in different types\n**************");
								yyerrorL(linea);
							}
						}
					}
				}
                        }               
| term			{
			   $$ = $1;
			}
;

term: term TIMES factor {
			   
				if($1->type == integer && $3->type== integer)
				{

					//Create the a temporal 
					entry_p actual; 
					union val value;
	    				value.i_value = 0;
					actual = NewItem(integer,theTable_p,tempC,value);
					tempC++;

					//We create the corresponding quad
					quadruple temp;
					union response resp;
					resp.rpt = actual;
					temp = NewQuad("TIMES",resp,$1,$3);	

					printQuad(temp);

					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;

					$$ = actual;
				}
				else
				{
					if($1->type == real && $3->type== integer)
					{
						//we check if the types are not the same, if the result variable 
						// is integer but the expression use float the cohersion produce
						// lost of precision.
						yyerror("+++++++++\nThe operation * has diferent types");
							//print warning
						yyerror("Error: cohersion will produce lost of precision\n**************");
						yyerrorL(linea);

						//Create the a temporal 
						entry_p actual; 
						union val value;
	    					value.r_value = 0.0;
						actual = NewItem(real,theTable_p,tempC,value);
						tempC++;

						//We create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = actual;
						temp = NewQuad("TIMES",resp,$1,$3);	

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;

						$$ = actual;
					}
					else
					{
						if($1->type == integer && $3->type== real){
							//we check if the types are not the same, if the result variable 
							// is integer but the expression use float the cohersion produce
							// lost of precision.
							yyerror("+++++++++\nThe operation * has diferent types");
								//print warning
							yyerror("Error: cohersion will produce lost of precision\n**************");
							yyerrorL(linea);

							//Create the a temporal 
							entry_p actual; 
							union val value;
	    						value.r_value = 0.0;
							actual = NewItem(real,theTable_p,tempC,value);
							tempC++;

							//We create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = actual;
							temp = NewQuad("TIMES",resp,$1,$3);	

							printQuad(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							$$ = actual;
					}
						else
						{
							if($1->type == real && $3->type== real)
							{
								//Create the a temporal 
								entry_p actual; 
								union val value;
	    							value.r_value = 0.0;
								actual = NewItem(real,theTable_p,tempC,value);
								tempC++;

								//We create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = actual;
								temp = NewQuad("TIMES",resp,$1,$3);	

								printQuad(temp);

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;

								$$ = actual;
							}
							else
							{
								yyerror("Error: in different types\n**************");
								yyerrorL(linea);
							}
						}
					}
				}
                        }                        
| term DIV factor       {
                           
				if($1->type == integer && $3->type== integer)
				{
					//Create the a temporal 
					entry_p actual; 
					union val value;
	    				value.i_value = 0;
					actual = NewItem(integer,theTable_p,tempC,value);
					tempC++;

					//We create the corresponding quad
					quadruple temp;
					union response resp;
					resp.rpt = actual;
					temp = NewQuad("DIV",resp,$1,$3);

					printQuad(temp);

					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;

					$$ = actual;	
				}
				else
				{
					if($1->type == real && $3->type== integer)
					{
						//we check if the types are not the same, if the result variable 
						// is integer but the expression use float the cohersion produce
						// lost of precision.
						yyerror("+++++++++\nThe operation / has diferent types");
							//print warning
						yyerror("Error: cohersion will produce lost of precision\n**************");
						yyerrorL(linea);

						//Create the a temporal 
						entry_p actual; 
						union val value;
	    					value.r_value = 0.0;
						actual = NewItem(real,theTable_p,tempC,value);
						tempC++;

						//We create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = actual;
						temp = NewQuad("DIV",resp,$1,$3);

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;

						$$ = actual;	
						}
					else
					{
						if($1->type == integer && $3->type== real){
							//we check if the types are not the same, if the result variable 
							// is integer but the expression use float the cohersion produce
							// lost of precision.
							yyerror("+++++++++\nThe operation / has diferent types");
								//print warning
							yyerror("Error: cohersion will produce lost of precision\n**************");
							yyerrorL(linea);

							//Create the a temporal 
							entry_p actual; 
							union val value;
	    						value.r_value = 0.0;
							actual = NewItem(real,theTable_p,tempC,value);
							tempC++;

							//We create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = actual;
							temp = NewQuad("DIV",resp,$1,$3);

							printQuad(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							$$ = actual;	
							}
						else
						{
							if($1->type == real && $3->type== real)
							{
								//Create the a temporal 
								entry_p actual; 
								union val value;
	    							value.r_value = 0.0;
								actual = NewItem(real,theTable_p,tempC,value);
								tempC++;

								//We create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = actual;
								temp = NewQuad("DIV",resp,$1,$3);

								printQuad(temp);

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;

								$$ = actual;	
							}
							else
							{
								yyerror("Error: in different types\n**************");
								yyerrorL(linea);
							}
						}
					}
				}
                        }                          
| factor		{
			   $$ = $1;
			}
;

factor: LPAREN exp RPAREN  {
				$$ = $2;
			   }
                  
| INT_NUM {
	    //Declaration of a temporal variable
	    entry_p actual; 

	    union val value;
	    value.i_value = $1;
	    actual = NewItem(integer,theTable_p,tempC,value);
	    tempC++;

	    $$ = actual;

          }
| FLOAT_NUM {
		//Declaration of a temporal variable
		entry_p actual; 

		//printf("real value%f\n",$1);
		union val value;
	    	value.r_value = $1;
	        actual = NewItem(real,theTable_p,tempC,value);
		tempC++;

	        $$ = actual;

            }
| variable  {
		$$ = $1;
            }
;

variable: ID {
                if(searchVar($1,theTable_p)==NULL){
		    yyerror("xxxxxxxxx\nThe variable was not declared\nxxxxxxxxx");
		    yyerrorL(linea);
                    //printf ("Error the variable %s was not declared\n",$1);
                }else{
		   $$ = searchVar($1,theTable_p);
		}
            }                                
;

epsilon:
;

%%

/**********************
 * Codigo C Adicional *
 **********************/

/* This is where the flex is included */
#include "lex.yy.c"

// we declare the yy functions
void yyerror (string input){
  printf ("%s\n",input);
  semantic_error=1;
}

void yyerrorL(int line){
  printf ("In the line:%d\n",line);
  printf("\n");
}

int main(int argc,char **argv)
{
    //Creas la tabla de Hash 
    theTable_p = g_hash_table_new(g_str_hash, g_str_equal);

    //Initialize the garray 
    garray = g_ptr_array_new();

    //Verifica que sea un argumento
    if (argc>1)
    yyin=fopen(argv[1],"rt");
    else
    //yyin=stdin;
    //Abre de forma de lectura el archivo que se va a compilar
    yyin=fopen("input.txt","rt");
    
    //Corres el codigo
    yyparse();
	int length;

    // Print al the quads that are generated
	
    // Print the entire  Hash table
    if(semantic_error==0){
    	printf("\n=================\n");
    	printf("NO SEMANTIC ERRORS\n");
    	printf("=================\n");
    }
    printf("\n\tPrint the entire table\n");
    /*
    if (PrintTable(theTable_p) != EXIT_SUCCESS)
    {
    	printf("Error while printing the symbol table");
    }*/
    
    printQuadTable(garray);

    //Now we are going to execute the quadruples
    theTable_p =execute_cycle(garray,theTable_p);
   
   printf("\n\tSecond Print the entire table\n");
    if (PrintTable(theTable_p) != EXIT_SUCCESS)
    {
    	printf("Error while printing the symbol table");
    }

    printf("Number of variables correctly declared: %d\n\n", number);

    //return DestroyTable(theTable_p);
}
