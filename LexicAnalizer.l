%{
#include <stdio.h> 
#include <stdlib.h>
#include <glib.h>  	

#define NUMPARAMS 2
	/* These variables are used for the comments and lines */
int coment=0;
int linea=1;
int declaracion=0;
       /*Variable used of the HashTable*/
GHashTable * theTable_p; 
%}

	/* This is the rule definition */
d	[0-9]
%%
	/* These variables shows 0 if a comment is close 1 if it is open  */
"/*" {coment=1;}  
"*/" {coment=0;}

	/* How each token is referenced */
"if"		{if(coment==0){return IF;}}
"then"		{if(coment==0){return THEN;}}
"else"		{if(coment==0){return ELSE;}}
"while"		{if(coment==0){return WHILE;}}
"do"		{if(coment==0){return DO;}}
":="		{if(coment==0){return ASSIGN;}}
"read"		{if(coment==0){return READ;}}
"int"		{if(coment==0){declaracion=1;return INTEGER;}}
"float"		{if(coment==0){declaracion=2;return FLOAT;}}
"("			{if(coment==0){return LPAREN;}}
")"			{if(coment==0){return RPAREN;}}
";"			{if(coment==0){return SEMICOLON;}}
"write"			{if(coment==0){return WRITE;}}
"{"			{if(coment==0){return LBRACKET;}}
"}"			{if(coment==0){return RBRACKET;}}
"<"			{if(coment==0){return LT;}}
">"			{if(coment==0){return MR;}}
"="			{if(coment==0){return EQ;}}
"+"			{if(coment==0){return PLUS;}}
"-"			{if(coment==0){return MINUS;}}
"*"			{if(coment==0){return TIMES;}}
"/"			{if(coment==0){return DIV;}}

	/* Enter/ Tabs / empty spaces*/
"\n"  {linea++;}
"\t" 
" " 
	/* All the notations for numbers*/
-?[0-9]+        {if(coment==0){yylval.integerValue=atoi(yytext);return INT_NUM;}}

-?(({d}+)|(({d}*\.{d}+)([eE][+-]?{d}+)?)) {if(coment==0) {
								yylval.floatValue=atof(yytext);
								return FLOAT_NUM;
							 }
					  }

(\/\*(\s*|.*?)*\*\/)|(\/\/.*) {}

	/* Individual IDs */
[A-Za-z_][A-Za-z0-9_]*  {
									if(coment==0){ 
										yylval.string=strdup(yytext);
										return ID;
									}
								}

	/* All other simbols*/
. 


%%




