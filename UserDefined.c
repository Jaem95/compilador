#include <stdlib.h>              /* Used for getc() and feof() functions */
#include <ctype.h>                    /* Used for the isdigit() function */
#include <stdio.h>                       /* Used to handle the FILE type */
#include <string.h>                                /* Used for strdup() */
#include <assert.h>                       /* Used for the asser function */
#include "UserDefined.h"

int PrintItem (entry_p theEntry_p)
{
	//Print The name of the variable
	printf("Name:%s\n",theEntry_p->name_p);
	
	//Print the Type of the variable
	if((int)theEntry_p->type == 3)
	{
		printf("Type: Float\n");
		printf("Value Float:%f\n",theEntry_p->value.r_value);
		
	}
	else
	{
		if((int)theEntry_p->type == 2){
			printf("Type: Integer\n");
			printf("Value Int:%d\n",theEntry_p->value.i_value);
		}
	}

	//Print the value of the variable
   	//printf("Value Int:%d\n",theEntry_p->value.i_value);
	//printf("Value Float:%f\n",theEntry_p->value.r_value);

	printf("----------------------------------\n");

	return EXIT_SUCCESS;
}

 
void SupportPrint (gpointer key_p, gpointer value_p, gpointer user_p)
{
	//Support table used to print all elemnts of the tabel
	PrintItem(value_p);
}

int PrintTable (GHashTable * theTable_p)
{
	//For each elemnt of the table 
	g_hash_table_foreach(theTable_p, SupportPrint, NULL);

	return EXIT_SUCCESS;
}

//Update the info of the hashTable
void UpdateData(char * s, enum myTypes type ,GHashTable * theTable_pp){
  	entry_p node_p;
    
	//Look the Hash table for the string 
	node_p = g_hash_table_lookup(theTable_pp, s);

	node_p->name_p = s;
	node_p->type = type;

	g_hash_table_replace(theTable_pp, node_p->name_p, node_p);
}

entry_p NewItem (enum myTypes type,GHashTable * theTable_pp,int i,union val v)
{
	//Declaration of the entry that will be created and returned
	entry_p actual;

	//Allocate memory for the entry
	actual = (tableEntry *) malloc(sizeof(tableEntry *));
        assert (actual != NULL);
	
	char * str= malloc(sizeof(char *));
        char * a = malloc(sizeof(char *));

        strcpy(str,"t");
        snprintf(a,sizeof(char *),"%d",i);
        strcat(str,a);

	//Assign the elements to the entry
	
	actual->name_p = str; 
	actual->type = type;
	if(type == integer){
		actual->value.i_value=v.i_value;
	}
	else{
		actual->value.r_value=v.r_value;	
	}

	g_hash_table_insert(theTable_pp, actual->name_p, actual);

    	//Return the entry
	return actual;
}

/*******
	Insert a new Quad
*******/
quadruple NewQuad (char * quadName_p, union response r, entry_p p1, entry_p p2)
{
	//Declaration of the quad that will be created and returned
	quadruple quadT;
	
	quadT = malloc(sizeof(quadruple));

	char * str= malloc(sizeof(char *));
	strcpy(str,quadName_p);

	quadT->op = strdup(str);
	quadT->resp = r;
	quadT->r1 = p1;
	quadT->r2 = p2;

	return quadT;
}

/*******
	Show a quad
*******/

void printQuad (quadruple q)
{
	printf("*********** Quad ***************\n");
	printf("Name:%s\n",q->op);
	printf("Result:%s\n",q->resp.rpt->name_p);
	printf("Arg1:%s\n",q->r1->name_p);
	if(q->r2 != NULL){
		printf("Arg2:%s\n",q->r2->name_p);
	}
	printf("\n");
}

void printQuadAddress (quadruple q)
{
	printf("*********** Quad statements ***************\n");
	printf("Name:%s\n",q->op);
	printf("Result:%d\n",q->resp.address);
	printf("\n");
}

/*******
	Search in hash table
*******/

entry_p searchVar(char * s, GHashTable * theTable_pp){

    entry_p node_p;
    
    //Look the Hash table for the string 
    node_p = g_hash_table_lookup(theTable_pp, s);
    return node_p;
}

void insertVar(enum myTypes type,char * s,GHashTable * theTable_pp){
	entry_p actual;

	//Allocate memory for the entry
	actual = (tableEntry *) malloc(sizeof(tableEntry *));
        assert (actual != NULL);

	//Assign the elements to the entry
	
	actual->name_p = s;
	actual->type = type;
	actual->value.i_value=0;
	actual->value.r_value=0;
	
	g_hash_table_insert(theTable_pp, actual->name_p, actual);
}

void backpatch(GPtrArray * actions,GPtrArray * list, int quad){
	long x = 0;	

	quadruple y;

	printf("*********** Llegue al backpatch ***************\n");
	
	for(int i=0; i<list->len;i++)
	{
		x = (long) g_ptr_array_index(list,i); 
		y = g_ptr_array_index(actions,x); 
		union response resp;
		y->resp.address = quad;
	}
}

GPtrArray * merge(GPtrArray * list1,GPtrArray * list2){
	GPtrArray * newList;
	newList = g_ptr_array_new();
	g_ptr_array_add(newList,list1);
	g_ptr_array_add(newList,list2);
	return newList;
}

GPtrArray *  copy(GPtrArray * list){
	GPtrArray * newList;
	long * x = malloc(sizeof(long));
	newList = g_ptr_array_new();

	for(int i=0;i<list->len;i++){
		x = (long *) g_ptr_array_index(list,i); 
		g_ptr_array_add (newList, (gpointer) x);
	}
	return newList;
}


GPtrArray * newList(int q){
	GPtrArray * newList;
	newList = g_ptr_array_new();
	g_ptr_array_add (newList, (gpointer) (long)q);
	return newList;
}


/*To free an entry 
First you have to deallocate the string(name) of the entry
  */
void FreeItem (entry_p theEntry_p)
{
	//Free the name
	free(theEntry_p->name_p);
	//Free the entry
   	free(theEntry_p);

}



GHashTable* execute_cycle(GPtrArray * garray,GHashTable * theTable_p)
{
    printf("\nExecuting Code %d\n",garray->len);
    char *string ="";
    quadruple aux;
    for(int index= 0; index < garray->len; index++)
    {
    		aux= g_ptr_array_index(garray,index);
    		string= aux->op;
        if (strcmp(string, "EQ") == 0) 
        {
          printf("Operator: eq\n");
	  entry_p arg1,arg2;
		
	  arg1 = searchVar(aux->r1->name_p,theTable_p);
	  arg2 = searchVar(aux->r2->name_p,theTable_p);

          if (arg1->type== integer)
          {	
          	 if (arg2->type== integer)
	          {
	          	if (arg1->value.i_value == arg2->value.i_value)
	          	{
	          		index = aux->resp.address; 
	          	}
	          }else{
	          	if (arg1->value.i_value == arg2->value.r_value)
	          	{
	          		index = aux->resp.address;
	          	}
	          }
          }else{
          	if (arg2->type== integer)
	          {
	          	if (arg1->value.r_value == arg2->value.i_value)
	          	{
	          		index = aux->resp.address;
	          	}
	          }else{
	          	if (arg1->value.r_value == arg2->value.r_value)
	          	{
	          		index = aux->resp.address;
	          	}
	          }
          }

			  g_hash_table_replace(theTable_p,arg1->name_p,arg1);
			  g_hash_table_replace(theTable_p,arg2->name_p,arg2);
        } 
        else if (strcmp(string, "TIMES") == 0)
        {
          printf("Operator: DIV\n");

	  entry_p r;
	  entry_p arg1,arg2;
		
	  r = searchVar(aux->resp.rpt->name_p,theTable_p);
	  arg1 = searchVar(aux->r1->name_p,theTable_p);
	  arg2 = searchVar(aux->r2->name_p,theTable_p);

          if(r->type == integer){
			  		if ((arg1->type== integer) && (arg2->type==integer))
					{
						//we assign the integer value to result variable of the operation r1* r2
						r->value.i_value= arg1->value.i_value * arg2->value.i_value;
					}
					//we check if the first variable is float and if the second variable is integer
			  		if ((arg1->type== real) && (arg2->type==integer))
					{
						//we assign the FLOAT value to result variable of the operation r1 * r2
			  			r->value.i_value= (int)arg1->value.r_value * arg2->value.i_value;
					}
			  		//we check if the first variable is integer and if the second variable is float
			  		if ((arg1->type== integer) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1* r2
			  			r->value.i_value= arg1->value.i_value * (int)arg2->value.r_value;
					}
			  			
			  }else{
			  		//we check if the 2 variables have a float type
			  		if ((arg1->type== real) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1* r2
			  			r->value.r_value= arg1->value.r_value * arg2->value.r_value;
					}
			  		//we check if the first variable is float and if the second variable is integer
			  		if ((arg1->type== real) && (arg2->type==integer))
					{
						//we assign the FLOAT value to result variable of the operation r1* r2
			  			r->value.r_value= arg1->value.r_value * (float)arg2->value.i_value;
					}
			  		//we check if the first variable is integer and if the second variable is float
			  		if ((arg1->type== integer) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1* r2
			  			r->value.r_value= (float)arg1->value.i_value * arg2->value.r_value;
					}
			  }

			  g_hash_table_replace(theTable_p,r->name_p,r);
			  g_hash_table_replace(theTable_p,arg1->name_p,arg1);
			  g_hash_table_replace(theTable_p,arg2->name_p,arg2);

        }
        else if (strcmp(string, "DIV") == 0)
        {
	   printf("Operator: DIV\n");

	  entry_p r;
	  entry_p arg1,arg2;
		
	  r = searchVar(aux->resp.rpt->name_p,theTable_p);
	  arg1 = searchVar(aux->r1->name_p,theTable_p);
	  arg2 = searchVar(aux->r2->name_p,theTable_p);

          if(r->type == integer){
			  		if ((arg1->type== integer) && (arg2->type==integer))
					{
						//we assign the integer value to result variable of the operation r1/ r2
						r->value.i_value= arg1->value.i_value / arg2->value.i_value;
					}
					//we check if the first variable is float and if the second variable is integer
			  		if ((arg1->type== real) && (arg2->type==integer))
					{
						//we assign the FLOAT value to result variable of the operation r1/r2
			  			r->value.i_value= (int)arg1->value.r_value / arg2->value.i_value;
					}
			  		//we check if the first variable is integer and if the second variable is float
			  		if ((arg1->type== integer) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1/ r2
			  			r->value.i_value= arg1->value.i_value / (int)arg2->value.r_value;
					}
			  			
			  }else{
			  		//we check if the 2 variables have a float type
			  		if ((arg1->type== real) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1/ r2
			  			r->value.r_value= arg1->value.r_value / arg2->value.r_value;
					}
			  		//we check if the first variable is float and if the second variable is integer
			  		if ((arg1->type== real) && (arg2->type==integer))
					{
						//we assign the FLOAT value to result variable of the operation r1/ r2
			  			r->value.r_value= arg1->value.r_value / (float)arg2->value.i_value;
					}
			  		//we check if the first variable is integer and if the second variable is float
			  		if ((arg1->type== integer) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1/ r2
			  			r->value.r_value= (float)arg1->value.i_value / arg2->value.r_value;
					}
			  }
          g_hash_table_replace(theTable_p,r->name_p,r);
			  g_hash_table_replace(theTable_p,arg1->name_p,arg1);
			  g_hash_table_replace(theTable_p,arg2->name_p,arg2);
        }
        else if (strcmp(string, "MINUS") == 0)
        {
          printf("Operator: MINUS\n");

	  entry_p r;
	  entry_p arg1,arg2;
		
	  r = searchVar(aux->resp.rpt->name_p,theTable_p);
	  arg1 = searchVar(aux->r1->name_p,theTable_p);
	  arg2 = searchVar(aux->r2->name_p,theTable_p);

          if(r->type == integer){
			  		if ((arg1->type== integer) && (arg2->type==integer))
					{
						//we assign the integer value to result variable of the operation r1- r2
						r->value.i_value= arg1->value.i_value - arg2->value.i_value;
					}
					//we check if the first variable is float and if the second variable is integer
			  		if ((arg1->type== real) && (arg2->type==integer))
					{
						//we assign the FLOAT value to result variable of the operation r1- r2
			  			r->value.i_value= (int)arg1->value.r_value - arg2->value.i_value;
					}
			  		//we check if the first variable is integer and if the second variable is float
			  		if ((arg1->type== integer) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1- r2
			  			r->value.i_value= arg1->value.i_value - (int)arg2->value.r_value;
					}
			  			
			  }else{
			  		//we check if the 2 variables have a float type
			  		if ((arg1->type== real) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1- r2
			  			r->value.r_value= arg1->value.r_value - arg2->value.r_value;
					}
			  		//we check if the first variable is float and if the second variable is integer
			  		if ((arg1->type== real) && (arg2->type==integer))
					{
						//we assign the FLOAT value to result variable of the operation r1- r2
			  			r->value.r_value= arg1->value.r_value - (float)arg2->value.i_value;
					}
			  		//we check if the first variable is integer and if the second variable is float
			  		if ((arg1->type== integer) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1- r2
			  			r->value.r_value= (float)arg1->value.i_value - arg2->value.r_value;
					}
			  }
			  g_hash_table_replace(theTable_p,r->name_p,r);
			  g_hash_table_replace(theTable_p,arg1->name_p,arg1);
			  g_hash_table_replace(theTable_p,arg2->name_p,arg2);

        }
        else if (strcmp(string, "PLUS") == 0)
        {
          printf("Operator: PLUS\n");

	  entry_p r;
	  entry_p arg1,arg2;
		
	  r = searchVar(aux->resp.rpt->name_p,theTable_p);
	  arg1 = searchVar(aux->r1->name_p,theTable_p);
	  arg2 = searchVar(aux->r2->name_p,theTable_p);

          if(r->type == integer){
			  		if ((arg1->type== integer) && (arg2->type==integer))
					{
						//we assign the integer value to result variable of the operation r1+ r2
						r->value.i_value= arg1->value.i_value + arg2->value.i_value;
					}
					//we check if the first variable is float and if the second variable is integer
			  		if ((arg1->type== real) && (arg2->type==integer))
					{
						//we assign the FLOAT value to result variable of the operation r1+ r2
			  			r->value.i_value= (int)arg1->value.r_value + arg2->value.i_value;
					}
			  		//we check if the first variable is integer and if the second variable is float
			  		if ((arg1->type== integer) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1+ r2
			  			r->value.i_value= arg1->value.i_value + (int)arg2->value.r_value;
					}
			  			
			  }else{
			  		//we check if the 2 variables have a float type
			  		if ((arg1->type== real) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1+ r2
			  			r->value.r_value= arg1->value.r_value + arg2->value.r_value;
					}
			  		//we check if the first variable is float and if the second variable is integer
			  		if ((arg1->type== real) && (arg2->type==integer))
					{
						//we assign the FLOAT value to result variable of the operation r1+ r2
			  			r->value.r_value= arg1->value.r_value + (float)arg2->value.i_value;
					}
			  		//we check if the first variable is integer and if the second variable is float
			  		if ((arg1->type== integer) && (arg2->type==real))
					{
						//we assign the FLOAT value to result variable of the operation r1+ r2
			  			r->value.r_value= (float)arg1->value.i_value + arg2->value.r_value;
					}
			  }
			  g_hash_table_replace(theTable_p,r->name_p,r);
			  g_hash_table_replace(theTable_p,arg1->name_p,arg1);
			  g_hash_table_replace(theTable_p,arg2->name_p,arg2);
        }
        else if (strcmp(string, "goto") == 0)
        {
          printf("Operator: goto\n");

	  index = aux->resp.address; 
        }
        else if (strcmp(string, "ASSIGN") == 0)
        {
          printf("Operator: assign\n");

	  entry_p r;
	  entry_p arg1;
		
	  r = searchVar(aux->resp.rpt->name_p,theTable_p);
	  arg1 = searchVar(aux->r1->name_p,theTable_p);

          if ((r->type== integer) && (arg1->type == integer)){
				r->value.i_value= arg1->value.i_value;
			  }
		          if ((r->type== integer) && (arg1->type == real)){
				r->value.i_value = (float)arg1->value.r_value;
			  }
		          if ((r->type== real) && (arg1->type == integer)){
				r->value.r_value= (int)arg1->value.r_value;
			  }
			  if ((r->type== real) && (arg1->type == real)){
				r->value.r_value = arg1->value.r_value;
			  }
			  g_hash_table_replace(theTable_p,r->name_p,r);
			  g_hash_table_replace(theTable_p,arg1->name_p,arg1);
        }
        else if (strcmp(string, "READ") == 0)
        {
		printf("\nInput: ");
		entry_p response;
		response = searchVar(aux->resp.rpt->name_p,theTable_p);
		if(response->type == integer){
			int n;
			scanf("%d",&response->value.i_value);
			response->value.i_value = n;
		}
		else
		{
			int f;
			scanf("%f",&response->value.r_value);
			response->value.r_value = f;
		}	
        }
        else if (strcmp(string, "WRITE") == 0)
        {

		entry_p response;
		response = searchVar(aux->resp.rpt->name_p,theTable_p);
		if(response->type == integer){
			printf("Output: %d", aux->resp.rpt->value.i_value); //here we gonna read a long	
		}
		else
		{
			printf("Output: %f", aux->resp.rpt->value.r_value); //here we gonna read a long	
		}
        }
        else if (strcmp(string, "LT") == 0)
        {
	printf("Operator:  less than\n");
		  entry_p arg1,arg2;
	
		  arg1 = searchVar(aux->r1->name_p,theTable_p);
		  arg2 = searchVar(aux->r2->name_p,theTable_p);

		  if (arg1->type== integer)
		  {	
		  	 if (arg2->type== integer)
			  {
			  	if (arg1->value.i_value < arg2->value.i_value)
			  	{
			  		index = aux->resp.address; 
			  	}
			  }else{
			  	if (arg1->value.i_value < arg2->value.r_value)
			  	{
			  		index = aux->resp.address;
			  	}
			  }
		  }else{
		  	if (arg2->type== integer)
			  {
			  	if (arg1->value.r_value < arg2->value.i_value)
			  	{
			  		index = aux->resp.address;
			  	}
			  }else{
			  	if (arg1->value.r_value < arg2->value.r_value)
			  	{
			  		index = aux->resp.address;
			  	}
			  }
		  }

			  g_hash_table_replace(theTable_p,arg1->name_p,arg1);
			  g_hash_table_replace(theTable_p,arg2->name_p,arg2);
        }
        else if (strcmp(string, "MR") == 0)
        {
	printf("Operator:  more than\n");
		  entry_p arg1,arg2;
	
		  arg1 = searchVar(aux->r1->name_p,theTable_p);
		  arg2 = searchVar(aux->r2->name_p,theTable_p);

		  if (arg1->type== integer)
		  {	
		  	 if (arg2->type== integer)
			  {
			  	if (arg1->value.i_value > arg2->value.i_value)
			  	{
			  		index = aux->resp.address; 
			  	}
			  }else{
			  	if (arg1->value.i_value > arg2->value.r_value)
			  	{
			  		index = aux->resp.address;
			  	}
			  }
		  }else{
		  	if (arg2->type== integer)
			  {
			  	if (arg1->value.r_value > arg2->value.i_value)
			  	{
			  		index = aux->resp.address;
			  	}
			  }else{
			  	if (arg1->value.r_value > arg2->value.r_value)
			  	{
			  		index = aux->resp.address;
			  	}
			  }
		  }
		 
			  g_hash_table_replace(theTable_p,arg1->name_p,arg1);
			  g_hash_table_replace(theTable_p,arg2->name_p,arg2);
        }

    }
   
   return theTable_p;
}

void printQuadTable(GPtrArray * garray){
	printf("\n---------------------------\n");
	printf("       Print Quad table\n");
	printf("---------------------------\n");
	for(int index= 0; index < garray->len; index++)
    {
    	quadruple aux;
	aux= g_ptr_array_index(garray,index);
	printf("index:%d\n",index);
	printf("op :%s\n",aux->op );
	if((strcmp(aux->op, "goto") == 0)){
		printf("Operator | Resp add - name - value \n");
		printf("%s	%d \n",aux->op,aux->resp.address);
	}
	else
	{
		if ((strcmp(aux->op, "MR") == 0) || (strcmp(aux->op, "LT") == 0) || (strcmp(aux->op, "EQ") == 0)){
			printf("Operator | Resp add - name - value | V1 name - value | V2 name - value  \n");
			printf("%s	%d	%s	%s\n",aux->op,aux->resp.address,aux->r1->name_p,aux->r2->name_p);
		}	
		else
		{
			if((strcmp(aux->op, "ASSIGN") == 0)){
				printf("Operator | Resp add - name - value | V1 name - value  \n");
				printf("%s	%s	%s	\n",aux->op,aux->resp.rpt->name_p,aux->r1->name_p);
			}
			else
			{
				if((strcmp(aux->op, "READ") == 0) || (strcmp(aux->op, "WRITE") == 0)){
					printf("Operator | Resp add - name - value \n");
					printf("%s	%s \n",aux->op,aux->resp.rpt->name_p);
				}
				else
				{
					printf("Operator | Resp add - name - value | V1 name - value | V2 name - value  \n");
					printf("%s	%s	%s	%s\n",aux->op,aux->resp.rpt->name_p,aux->r1->name_p,aux->r2->name_p);
				}
			}
		}
	}

    }
}
/*Used to destroy the hash table
int DestroyTable (GHashTable * theTable_p)
{
	//Remove every entry in the table
	g_hash_table_remove_all (theTable_p);

	//Release the list
	free(theTable_p);

	return EXIT_SUCCESS;
}*/
