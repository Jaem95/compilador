Sintatic Analizer and Semantic Analizer and Code Generator
=============================================================================

Lab for Compiler Design,Spring 2017

Created by:
Jose Luis Santillan
Jose Antonio Escobar Macias
Oscar Alejandro Sanabria Barradas 

on 05/05/2017.

Copyright © 2017. All rights reserved.
=============================================================================

#Instructions

Just run the build.sh and then execute the file Analizer with the file text that you want to analize.

=============================================================================
#Commands

To build:
sh Build.sh

To run the exe:
./Analizer *file to compile*(without asterisks)
=============================================================================
#####################
#  IMPORTANT NOTE:  #
#################################################################################
#																										  #
# THE PROGRAM WAS TESTED WITH THE FILE INPUT.TXT AND THE PROGRAM RUN CORRECTLY. #
# 																										  #
#################################################################################

=============================================================================
#Syntax
For semantic error:
	*****Error: *****
	error_message
	*****************
	error line
For warning:
	++++++++++++
	warning_message
	++++++++++++
	error line
For not variable declared:
	xxxxxxxxxxxx
	error_message
	xxxxxxxxxxxx
	error line



