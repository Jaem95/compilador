/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NAME = 258,
     IF = 259,
     THEN = 260,
     ELSE = 261,
     WHILE = 262,
     DO = 263,
     ASSIGN = 264,
     READ = 265,
     LPAREN = 266,
     RPAREN = 267,
     SEMICOLON = 268,
     WRITE = 269,
     LBRACKET = 270,
     RBRACKET = 271,
     LT = 272,
     MR = 273,
     EQ = 274,
     PLUS = 275,
     MINUS = 276,
     TIMES = 277,
     DIV = 278,
     ID = 279,
     INTEGER = 280,
     FLOAT = 281,
     INT_NUM = 282,
     FLOAT_NUM = 283
   };
#endif
/* Tokens.  */
#define NAME 258
#define IF 259
#define THEN 260
#define ELSE 261
#define WHILE 262
#define DO 263
#define ASSIGN 264
#define READ 265
#define LPAREN 266
#define RPAREN 267
#define SEMICOLON 268
#define WRITE 269
#define LBRACKET 270
#define RBRACKET 271
#define LT 272
#define MR 273
#define EQ 274
#define PLUS 275
#define MINUS 276
#define TIMES 277
#define DIV 278
#define ID 279
#define INTEGER 280
#define FLOAT 281
#define INT_NUM 282
#define FLOAT_NUM 283




/* Copy the first part of user declarations.  */
#line 9 "SintacticAnalizer.y"

    
    /**********************
     * Declaraciones en C *
     **********************/
    
    
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "UserDefined.h"
    #include <glib.h>   
    #include <assert.h>           

    extern int yylex(void);
    extern char *yytext;
    extern int linea;
    int error2=0;
    int error3=0;
    extern FILE *yyin;
    
    int number=0;
    int semantic_error =0;

    //Función de error
    void yyerror(string input);
    void yyerrorL(int line);

    //Funcion de busqueda
    //void symlook(char * s, int type);
   

   // Declaración de la tabla de hash
   extern GHashTable * theTable_p; 

   //Quad control
   int quadC = 0;
   int tempC = 0;

    GPtrArray * garray;

    //int correct=0, lineno=1;



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 60 "SintacticAnalizer.y"
{
    entry_p entry;
    char * string;
    int integerValue;
    float floatValue;
}
/* Line 193 of yacc.c.  */
#line 204 "SintacticAnalizer.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 217 "SintacticAnalizer.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   72

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  29
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  16
/* YYNRULES -- Number of rules.  */
#define YYNRULES  35
/* YYNRULES -- Number of states.  */
#define YYNSTATES  72

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   283

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint8 yyprhs[] =
{
       0,     0,     3,     6,     9,    11,    15,    17,    19,    23,
      25,    31,    40,    47,    52,    58,    64,    66,    67,    69,
      73,    77,    81,    85,    87,    91,    95,    97,   101,   105,
     107,   111,   113,   115,   117,   119
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      30,     0,    -1,    31,    34,    -1,    31,    32,    -1,    44,
      -1,    33,    24,    13,    -1,    25,    -1,    26,    -1,    34,
      35,    36,    -1,    44,    -1,     4,    39,     5,    36,    35,
      -1,     4,    39,     5,    36,    35,    37,    36,    35,    -1,
       7,    36,    39,     8,    36,    35,    -1,    43,     9,    39,
      13,    -1,    10,    11,    43,    12,    13,    -1,    14,    11,
      39,    12,    13,    -1,    38,    -1,    -1,     6,    -1,    15,
      34,    16,    -1,    40,    17,    40,    -1,    40,    18,    40,
      -1,    40,    19,    40,    -1,    40,    -1,    40,    20,    41,
      -1,    40,    21,    41,    -1,    41,    -1,    41,    22,    42,
      -1,    41,    23,    42,    -1,    42,    -1,    11,    39,    12,
      -1,    27,    -1,    28,    -1,    43,    -1,    24,    -1,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   108,   108,   113,   114,   116,   131,   134,   139,   145,
     150,   157,   165,   190,   277,   289,   301,   307,   312,   319,
     326,   360,   394,   427,   432,   552,   671,   676,   797,   917,
     922,   926,   938,   951,   956,   967
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NAME", "IF", "THEN", "ELSE", "WHILE",
  "DO", "ASSIGN", "READ", "LPAREN", "RPAREN", "SEMICOLON", "WRITE",
  "LBRACKET", "RBRACKET", "LT", "MR", "EQ", "PLUS", "MINUS", "TIMES",
  "DIV", "ID", "INTEGER", "FLOAT", "INT_NUM", "FLOAT_NUM", "$accept",
  "program", "var_dec", "single_dec", "type", "stmt_seq", "stmt", "m", "n",
  "block", "exp", "simple_exp", "term", "factor", "variable", "epsilon", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    29,    30,    31,    31,    32,    33,    33,    34,    34,
      35,    35,    35,    35,    35,    35,    35,    36,    37,    38,
      39,    39,    39,    39,    40,    40,    40,    41,    41,    41,
      42,    42,    42,    42,    43,    44
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     2,     1,     3,     1,     1,     3,     1,
       5,     8,     6,     4,     5,     5,     1,     0,     1,     3,
       3,     3,     3,     1,     3,     3,     1,     3,     3,     1,
       3,     1,     1,     1,     1,     0
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      35,     0,    35,     4,     1,     6,     7,     3,     0,     2,
       9,     0,     0,    17,     0,     0,    35,    34,    17,    16,
       0,     5,     0,    31,    32,     0,    23,    26,    29,    33,
       0,     0,     0,     0,     8,     0,     0,    17,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    19,     0,
      30,     0,    20,    21,    22,    24,    25,    27,    28,    17,
       0,     0,    13,    10,     0,    14,    15,    18,    17,    12,
       0,    11
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     1,     2,     7,     8,     9,    18,    30,    68,    19,
      25,    26,    27,    28,    29,    10
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -45
static const yytype_int8 yypact[] =
{
     -45,     1,   -21,   -45,   -45,   -45,   -45,   -45,     3,    21,
     -45,     6,     5,   -45,    -3,    23,   -45,   -45,   -45,   -45,
      28,   -45,     5,   -45,   -45,    25,    39,    -5,   -45,   -45,
       5,    17,     5,    -1,   -45,     5,    32,   -45,     5,     5,
       5,     5,     5,     5,     5,    54,    35,    51,   -45,    52,
     -45,    21,    19,    19,    19,    -5,    -5,   -45,   -45,   -45,
      53,    55,   -45,    58,    21,   -45,   -45,   -45,   -45,   -45,
      21,   -45
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -45,   -45,   -45,   -45,   -45,    56,   -44,   -16,   -45,   -45,
      16,   -28,     8,    10,    -9,    67
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      20,     4,    34,    12,     5,     6,    13,    63,    31,    14,
      52,    53,    54,    15,    16,    48,    22,    43,    44,    21,
      69,    51,    46,    17,    20,    12,    71,    11,    13,    17,
      37,    14,    23,    24,    32,    15,    16,    35,    36,    41,
      42,    17,    20,    64,    50,    17,    45,    60,    47,    55,
      56,    49,    70,    57,    58,    20,    38,    39,    40,    41,
      42,    20,    59,    61,    67,    62,    65,     3,    66,     0,
       0,     0,    33
};

static const yytype_int8 yycheck[] =
{
       9,     0,    18,     4,    25,    26,     7,    51,    11,    10,
      38,    39,    40,    14,    15,    16,    11,    22,    23,    13,
      64,    37,    31,    24,    33,     4,    70,    24,     7,    24,
       5,    10,    27,    28,    11,    14,    15,     9,    22,    20,
      21,    24,    51,    59,    12,    24,    30,    12,    32,    41,
      42,    35,    68,    43,    44,    64,    17,    18,    19,    20,
      21,    70,     8,    12,     6,    13,    13,     0,    13,    -1,
      -1,    -1,    16
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    30,    31,    44,     0,    25,    26,    32,    33,    34,
      44,    24,     4,     7,    10,    14,    15,    24,    35,    38,
      43,    13,    11,    27,    28,    39,    40,    41,    42,    43,
      36,    11,    11,    34,    36,     9,    39,     5,    17,    18,
      19,    20,    21,    22,    23,    39,    43,    39,    16,    39,
      12,    36,    40,    40,    40,    41,    41,    42,    42,     8,
      12,    12,    13,    35,    36,    13,    13,     6,    37,    35,
      36,    35
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 108 "SintacticAnalizer.y"
    {
				
			 ;}
    break;

  case 5:
#line 116 "SintacticAnalizer.y"
    {
                                if(searchVar((yyvsp[(2) - (3)].string),theTable_p)==NULL){/*Check if the variable is already declared*/
                                		//insert varible in the hash table
                                   insertVar((yyvsp[(1) - (3)].integerValue),(yyvsp[(2) - (3)].string),theTable_p); 
                                   //number of variables declared
                                   number++;
                                }else{
                                	//print error when a variable is already declared
					yyerror("Double declaration");
					//print the line where error was found
					yyerrorL(linea);
                                }
                              ;}
    break;

  case 6:
#line 131 "SintacticAnalizer.y"
    {
                (yyval.integerValue)=integer;
            ;}
    break;

  case 7:
#line 134 "SintacticAnalizer.y"
    {
                (yyval.integerValue)=real;
            ;}
    break;

  case 8:
#line 139 "SintacticAnalizer.y"
    {
				//printf("M values%d\n",$3);
				//printf("%d",$2->list_nxt->len);
				//$$->list_nxt = copy($2->list_nxt);
				//backpatch(garray,$2->list_nxt, $3);
		       ;}
    break;

  case 9:
#line 145 "SintacticAnalizer.y"
    {

	   ;}
    break;

  case 10:
#line 150 "SintacticAnalizer.y"
    {

			(yyval.entry)=malloc(sizeof(entry_p));

			backpatch(garray,(yyvsp[(2) - (5)].entry)->list_t,(yyvsp[(4) - (5)].integerValue));
			(yyval.entry)->list_nxt = merge((yyvsp[(2) - (5)].entry)->list_f,(yyvsp[(5) - (5)].entry)->list_nxt);
		      ;}
    break;

  case 11:
#line 157 "SintacticAnalizer.y"
    {
					(yyval.entry)=malloc(sizeof(entry_p));

					backpatch(garray,(yyvsp[(2) - (8)].entry)->list_t,(yyvsp[(4) - (8)].integerValue));
					backpatch(garray,(yyvsp[(2) - (8)].entry)->list_f,(yyvsp[(7) - (8)].integerValue));

					(yyval.entry)->list_nxt = merge((yyvsp[(5) - (8)].entry)->list_nxt,merge((yyvsp[(6) - (8)].entry)->list_nxt,(yyvsp[(8) - (8)].entry)->list_nxt));
				  ;}
    break;

  case 12:
#line 165 "SintacticAnalizer.y"
    {
				(yyval.entry)=malloc(sizeof(entry_p));
				long x;
				//x = (long)g_ptr_array_index($3->list_t,0);
				//printf(" list true: %ld\n",x); 

				//x = (long)g_ptr_array_index($3->list_f,0);
				//printf(" list false: %ld\n",x); 

				//printf("M values%d\n",$5);

				backpatch(garray,(yyvsp[(3) - (6)].entry)->list_t,(yyvsp[(5) - (6)].integerValue));

				(yyval.entry)->list_nxt = (yyvsp[(3) - (6)].entry)->list_f;

				quadruple temp;
				union response resp;
				resp.address = (yyvsp[(2) - (6)].integerValue);
				temp = NewQuad("goto",resp,NULL,NULL);
				printQuadAddress(temp);
				g_ptr_array_add (garray, (gpointer) temp);
				quadC++;


			     ;}
    break;

  case 13:
#line 190 "SintacticAnalizer.y"
    {
						if((yyvsp[(1) - (4)].entry)->type == integer && (yyvsp[(3) - (4)].entry)->type== integer)
						{
							//In case both have the same types we create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = (yyvsp[(1) - (4)].entry);
							temp = NewQuad("ASSIGN",resp,(yyvsp[(3) - (4)].entry),NULL);

							printQuad(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;
	
						}
						else
						{
							if((yyvsp[(1) - (4)].entry)->type == real && (yyvsp[(3) - (4)].entry)->type== integer)
							{
								//we check if the types are not the same, if the result variable 
								// is integer but the expression use float the cohersion produce
								// lost of precision.
								yyerror("*****Error: *****\nThe operation result := is float");
									//print warning
								yyerror("Error: cohersion will produce lost of precision\n**************");
								yyerrorL(linea);

								//create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = (yyvsp[(1) - (4)].entry);
								temp = NewQuad("ASSIGN",resp,(yyvsp[(3) - (4)].entry),NULL);

								printQuad(temp);

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;
								}
							else
							{
								if((yyvsp[(1) - (4)].entry)->type == integer && (yyvsp[(3) - (4)].entry)->type== real){
									//we check if the types are not the same, if the result variable 
									// is integer but the expression use float the cohersion produce
									// lost of precision.
									yyerror("*****Error: *****\nThe operation result := is integer");
										//print warning
									yyerror("Error: cohersion will produce lost of precision\n**************");
									yyerrorL(linea);

									//create the corresponding quad
									quadruple temp;
									union response resp;
									resp.rpt = (yyvsp[(1) - (4)].entry);
									temp = NewQuad("ASSIGN",resp,(yyvsp[(3) - (4)].entry),NULL);

									printQuad(temp);

									g_ptr_array_add (garray, (gpointer) temp);
									quadC++;
									}
								else
								{
									if((yyvsp[(1) - (4)].entry)->type == real && (yyvsp[(3) - (4)].entry)->type== real)
									{
							
										//In case both have the same types we create the corresponding quad
										quadruple temp;
										union response resp;
										resp.rpt = (yyvsp[(1) - (4)].entry);
										temp = NewQuad("ASSIGN",resp,(yyvsp[(3) - (4)].entry),NULL);

										printQuad(temp);

										g_ptr_array_add (garray, (gpointer) temp);
										quadC++;
									}
									else
									{
										yyerror("Error: in different types\n**************");
										yyerrorL(linea);
									}
								}
							}
						}
					;}
    break;

  case 14:
#line 277 "SintacticAnalizer.y"
    {
						//create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = (yyvsp[(3) - (5)].entry);
						temp = NewQuad("READ",resp,NULL,NULL);

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;
					     ;}
    break;

  case 15:
#line 289 "SintacticAnalizer.y"
    {
						//create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = (yyvsp[(3) - (5)].entry);
						temp = NewQuad("WITE",resp,NULL,NULL);

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);	
						quadC++;
					 ;}
    break;

  case 16:
#line 301 "SintacticAnalizer.y"
    {
		(yyval.entry)=malloc(sizeof(entry_p));
		(yyval.entry) = (yyvsp[(1) - (1)].entry);
	     ;}
    break;

  case 17:
#line 307 "SintacticAnalizer.y"
    {
	(yyval.integerValue) = quadC + 1;
  ;}
    break;

  case 18:
#line 312 "SintacticAnalizer.y"
    {
	(yyval.entry)=malloc(sizeof(entry_p));
	(yyval.entry)->list_nxt = newList(quadC);
      ;}
    break;

  case 19:
#line 319 "SintacticAnalizer.y"
    {
					(yyval.entry)=malloc(sizeof(entry_p));
					(yyval.entry) = (yyvsp[(2) - (3)].entry);
			   	      ;}
    break;

  case 20:
#line 326 "SintacticAnalizer.y"
    {
						(yyval.entry)=malloc(sizeof(entry_p));

						//we check the types of the variables are the same
						if((yyvsp[(1) - (3)].entry)->type != (yyvsp[(3) - (3)].entry)->type){
							yyerror("*****Error: *****\nThe operation < has diferent types\n**************");
							yyerrorL(linea);
						}
						else{
							//In case both have the same types we create the corresponding quad
							quadruple temp;
							union response resp;
							resp.address = 3;
							temp = NewQuad("LT",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

							printQuadAddress(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							//True list for the production
							(yyval.entry)->list_t = newList(quadC);

							//False List for the production
							(yyval.entry)->list_f = newList(quadC+1);

							resp.address = 0;
							temp = NewQuad("goto",resp,NULL,NULL);
							printQuadAddress(temp);
							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

						}
			      ;}
    break;

  case 21:
#line 360 "SintacticAnalizer.y"
    {
						(yyval.entry)=malloc(sizeof(entry_p));

						//we check the types of the variables are the same
						if((yyvsp[(1) - (3)].entry)->type != (yyvsp[(3) - (3)].entry)->type){
							yyerror("*****Error: *****\nThe operation > has diferent types\n**************");
							yyerrorL(linea);
						}
						else{
							//In case both have the same types we create the corresponding quad
							quadruple temp;
							union response resp;
							resp.address = 0;
							temp = NewQuad("MR",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));
							
							printQuadAddress(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							//True list for the production
							(yyval.entry)->list_t = newList(quadC);

							//False List for the production
							(yyval.entry)->list_f = newList(quadC+1);

							resp.address = 0;
							temp = NewQuad("goto",resp,NULL,NULL);
							printQuadAddress(temp);
							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;
						}

			   ;}
    break;

  case 22:
#line 394 "SintacticAnalizer.y"
    {
				(yyval.entry)=malloc(sizeof(entry_p));

				//we check the types of the variables are the same
				if((yyvsp[(1) - (3)].entry)->type != (yyvsp[(3) - (3)].entry)->type){
					yyerror("*****Error: *****\nThe operation = has diferent types\n**************");
					yyerrorL(linea);
				}
				else{
					//In case both have the same types we create the corresponding quad
					quadruple temp;
					union response resp;
					resp.address = 0;
					temp = NewQuad("EQ",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

					printQuadAddress(temp);

					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;

					//True list for the production
					(yyval.entry)->list_t = newList(quadC);

					//False List for the production
					(yyval.entry)->list_f = newList(quadC+1);

					resp.address = 0;
					temp = NewQuad("goto",resp,NULL,NULL);
					printQuadAddress(temp);
					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;
				}
			   ;}
    break;

  case 23:
#line 427 "SintacticAnalizer.y"
    {
			   	(yyval.entry) = (yyvsp[(1) - (1)].entry);
			   ;}
    break;

  case 24:
#line 432 "SintacticAnalizer.y"
    {
					if((yyvsp[(1) - (3)].entry)->type == integer && (yyvsp[(3) - (3)].entry)->type== integer)
					{

						//Create the a temporal 
						entry_p actual; 
						union val value;
	    					value.i_value = 0;
						actual = NewItem(integer,theTable_p,tempC,value);
						tempC++;

						//We create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = actual;
						temp = NewQuad("PLUS",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

						printQuad(temp);	

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;

						(yyval.entry) = actual;
					}
					else
					{
						if((yyvsp[(1) - (3)].entry)->type == real && (yyvsp[(3) - (3)].entry)->type== integer)
						{
							//we check if the types are not the same, if the result variable 
							// is integer but the expression use float the cohersion produce
							// lost of precision.
							yyerror("+++++++++\nThe operation + has diferent types");
								//print warning
							yyerror("Error: cohersion will produce lost of precision\n**************");
							yyerrorL(linea);

							//Create the a temporal 
							entry_p actual; 
							union val value;
	    						value.r_value = 0.0;
							actual = NewItem(real,theTable_p,tempC,value);
							tempC++;

							//We create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = actual;
							temp = NewQuad("PLUS",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

							printQuad(temp);	

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;
							(yyval.entry) = actual;
						}
						else
						{
							if((yyvsp[(1) - (3)].entry)->type == integer && (yyvsp[(3) - (3)].entry)->type== real){
								//we check if the types are not the same, if the result variable 
								// is integer but the expression use float the cohersion produce
								// lost of precision.
								yyerror("+++++++++\nThe operation + has diferent types");
									//print warning
								yyerror("Error: cohersion will produce lost of precision\n**************");
								yyerrorL(linea);

								//Create the a temporal 
								entry_p actual; 
								union val value;
	    							value.r_value = 0.0;
								actual = NewItem(real,theTable_p,tempC,value);
								tempC++;

								//We create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = actual;
								temp = NewQuad("PLUS",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

								printQuad(temp);	

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;

								(yyval.entry) = actual;
						}
							else
							{
								if((yyvsp[(1) - (3)].entry)->type == real && (yyvsp[(3) - (3)].entry)->type== real)
								{
									//Create the a temporal 
									entry_p actual; 
									union val value;
	    								value.r_value = 0.0;
									actual = NewItem(real,theTable_p,tempC,value);
									tempC++;

									//We create the corresponding quad
									quadruple temp;
									union response resp;
									resp.rpt = actual;
									temp = NewQuad("PLUS",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

									printQuad(temp);	

									g_ptr_array_add (garray, (gpointer) temp);
									quadC++;

									(yyval.entry) = actual;
								}
								else
								{
									yyerror("Error: in different types\n**************");
									yyerrorL(linea);
								}
							}
						}
					}
			            ;}
    break;

  case 25:
#line 552 "SintacticAnalizer.y"
    {
			   if((yyvsp[(1) - (3)].entry)->type == integer && (yyvsp[(3) - (3)].entry)->type== integer)
				{
					//Create the a temporal 
					entry_p actual; 
					union val value;
	    				value.i_value = 0;
					actual = NewItem(integer,theTable_p,tempC,value);
					tempC++;

					//We create the corresponding quad
					quadruple temp;
					union response resp;
					resp.rpt = actual;
					temp = NewQuad("MINUS",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

					printQuad(temp);
					
					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;

					(yyval.entry) = actual;
				}
				else
				{
					if((yyvsp[(1) - (3)].entry)->type == real && (yyvsp[(3) - (3)].entry)->type== integer)
					{
						//we check if the types are not the same, if the result variable 
						// is integer but the expression use float the cohersion produce
						// lost of precision.
						yyerror("+++++++++\nThe operation - has diferent types");
							//print warning
						yyerror("Error: cohersion will produce lost of precision\n**************");
						yyerrorL(linea);

						//Create the a temporal 
						entry_p actual; 
						union val value;
	    					value.r_value = 0.0;
						actual = NewItem(real,theTable_p,tempC,value);
						tempC++;

						//We create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = actual;
						temp = NewQuad("MINUS",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;

						(yyval.entry) = actual;
					}
					else
					{
						if((yyvsp[(1) - (3)].entry)->type == integer && (yyvsp[(3) - (3)].entry)->type== real){
							//we check if the types are not the same, if the result variable 
							// is integer but the expression use float the cohersion produce
							// lost of precision.
							yyerror("+++++++++\nThe operation - has diferent types");
								//print warning
							yyerror("Error: cohersion will produce lost of precision\n**************");
							yyerrorL(linea);
							
							//Create the a temporal 
							entry_p actual; 
							union val value;
	    						value.r_value = 0.0;
							actual = NewItem(real,theTable_p,tempC,value);
							tempC++;

							//We create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = actual;
							temp = NewQuad("MINUS",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

							printQuad(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							(yyval.entry) = actual;
					}
						else
						{
							if((yyvsp[(1) - (3)].entry)->type == real && (yyvsp[(3) - (3)].entry)->type== real)
							{
								//Create the a temporal 
								entry_p actual; 
								union val value;
	    							value.r_value = 0.0;
								actual = NewItem(real,theTable_p,tempC,value);
								tempC++;

								//We create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = actual;
								temp = NewQuad("MINUS",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

								printQuad(temp);

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;

								(yyval.entry) = actual;
							}
							else
							{
								yyerror("Error: in different types\n**************");
								yyerrorL(linea);
							}
						}
					}
				}
                        ;}
    break;

  case 26:
#line 671 "SintacticAnalizer.y"
    {
			   (yyval.entry) = (yyvsp[(1) - (1)].entry);
			;}
    break;

  case 27:
#line 676 "SintacticAnalizer.y"
    {
			   
				if((yyvsp[(1) - (3)].entry)->type == integer && (yyvsp[(3) - (3)].entry)->type== integer)
				{

					//Create the a temporal 
					entry_p actual; 
					union val value;
	    				value.i_value = 0;
					actual = NewItem(integer,theTable_p,tempC,value);
					tempC++;

					//We create the corresponding quad
					quadruple temp;
					union response resp;
					resp.rpt = actual;
					temp = NewQuad("TIMES",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));	

					printQuad(temp);

					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;

					(yyval.entry) = actual;
				}
				else
				{
					if((yyvsp[(1) - (3)].entry)->type == real && (yyvsp[(3) - (3)].entry)->type== integer)
					{
						//we check if the types are not the same, if the result variable 
						// is integer but the expression use float the cohersion produce
						// lost of precision.
						yyerror("+++++++++\nThe operation * has diferent types");
							//print warning
						yyerror("Error: cohersion will produce lost of precision\n**************");
						yyerrorL(linea);

						//Create the a temporal 
						entry_p actual; 
						union val value;
	    					value.r_value = 0.0;
						actual = NewItem(real,theTable_p,tempC,value);
						tempC++;

						//We create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = actual;
						temp = NewQuad("TIMES",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));	

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;

						(yyval.entry) = actual;
					}
					else
					{
						if((yyvsp[(1) - (3)].entry)->type == integer && (yyvsp[(3) - (3)].entry)->type== real){
							//we check if the types are not the same, if the result variable 
							// is integer but the expression use float the cohersion produce
							// lost of precision.
							yyerror("+++++++++\nThe operation * has diferent types");
								//print warning
							yyerror("Error: cohersion will produce lost of precision\n**************");
							yyerrorL(linea);

							//Create the a temporal 
							entry_p actual; 
							union val value;
	    						value.r_value = 0.0;
							actual = NewItem(real,theTable_p,tempC,value);
							tempC++;

							//We create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = actual;
							temp = NewQuad("TIMES",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));	

							printQuad(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							(yyval.entry) = actual;
					}
						else
						{
							if((yyvsp[(1) - (3)].entry)->type == real && (yyvsp[(3) - (3)].entry)->type== real)
							{
								//Create the a temporal 
								entry_p actual; 
								union val value;
	    							value.r_value = 0.0;
								actual = NewItem(real,theTable_p,tempC,value);
								tempC++;

								//We create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = actual;
								temp = NewQuad("TIMES",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));	

								printQuad(temp);

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;

								(yyval.entry) = actual;
							}
							else
							{
								yyerror("Error: in different types\n**************");
								yyerrorL(linea);
							}
						}
					}
				}
                        ;}
    break;

  case 28:
#line 797 "SintacticAnalizer.y"
    {
                           
				if((yyvsp[(1) - (3)].entry)->type == integer && (yyvsp[(3) - (3)].entry)->type== integer)
				{
					//Create the a temporal 
					entry_p actual; 
					union val value;
	    				value.i_value = 0;
					actual = NewItem(integer,theTable_p,tempC,value);
					tempC++;

					//We create the corresponding quad
					quadruple temp;
					union response resp;
					resp.rpt = actual;
					temp = NewQuad("DIV",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

					printQuad(temp);

					g_ptr_array_add (garray, (gpointer) temp);
					quadC++;

					(yyval.entry) = actual;	
				}
				else
				{
					if((yyvsp[(1) - (3)].entry)->type == real && (yyvsp[(3) - (3)].entry)->type== integer)
					{
						//we check if the types are not the same, if the result variable 
						// is integer but the expression use float the cohersion produce
						// lost of precision.
						yyerror("+++++++++\nThe operation / has diferent types");
							//print warning
						yyerror("Error: cohersion will produce lost of precision\n**************");
						yyerrorL(linea);

						//Create the a temporal 
						entry_p actual; 
						union val value;
	    					value.r_value = 0.0;
						actual = NewItem(real,theTable_p,tempC,value);
						tempC++;

						//We create the corresponding quad
						quadruple temp;
						union response resp;
						resp.rpt = actual;
						temp = NewQuad("DIV",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

						printQuad(temp);

						g_ptr_array_add (garray, (gpointer) temp);
						quadC++;

						(yyval.entry) = actual;	
						}
					else
					{
						if((yyvsp[(1) - (3)].entry)->type == integer && (yyvsp[(3) - (3)].entry)->type== real){
							//we check if the types are not the same, if the result variable 
							// is integer but the expression use float the cohersion produce
							// lost of precision.
							yyerror("+++++++++\nThe operation / has diferent types");
								//print warning
							yyerror("Error: cohersion will produce lost of precision\n**************");
							yyerrorL(linea);

							//Create the a temporal 
							entry_p actual; 
							union val value;
	    						value.r_value = 0.0;
							actual = NewItem(real,theTable_p,tempC,value);
							tempC++;

							//We create the corresponding quad
							quadruple temp;
							union response resp;
							resp.rpt = actual;
							temp = NewQuad("DIV",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

							printQuad(temp);

							g_ptr_array_add (garray, (gpointer) temp);
							quadC++;

							(yyval.entry) = actual;	
							}
						else
						{
							if((yyvsp[(1) - (3)].entry)->type == real && (yyvsp[(3) - (3)].entry)->type== real)
							{
								//Create the a temporal 
								entry_p actual; 
								union val value;
	    							value.r_value = 0.0;
								actual = NewItem(real,theTable_p,tempC,value);
								tempC++;

								//We create the corresponding quad
								quadruple temp;
								union response resp;
								resp.rpt = actual;
								temp = NewQuad("DIV",resp,(yyvsp[(1) - (3)].entry),(yyvsp[(3) - (3)].entry));

								printQuad(temp);

								g_ptr_array_add (garray, (gpointer) temp);
								quadC++;

								(yyval.entry) = actual;	
							}
							else
							{
								yyerror("Error: in different types\n**************");
								yyerrorL(linea);
							}
						}
					}
				}
                        ;}
    break;

  case 29:
#line 917 "SintacticAnalizer.y"
    {
			   (yyval.entry) = (yyvsp[(1) - (1)].entry);
			;}
    break;

  case 30:
#line 922 "SintacticAnalizer.y"
    {
				(yyval.entry) = (yyvsp[(2) - (3)].entry);
			   ;}
    break;

  case 31:
#line 926 "SintacticAnalizer.y"
    {
	    //Declaration of a temporal variable
	    entry_p actual; 

	    union val value;
	    value.i_value = (yyvsp[(1) - (1)].integerValue);
	    actual = NewItem(integer,theTable_p,tempC,value);
	    tempC++;

	    (yyval.entry) = actual;

          ;}
    break;

  case 32:
#line 938 "SintacticAnalizer.y"
    {
		//Declaration of a temporal variable
		entry_p actual; 

		//printf("real value%f\n",$1);
		union val value;
	    	value.r_value = (yyvsp[(1) - (1)].floatValue);
	        actual = NewItem(real,theTable_p,tempC,value);
		tempC++;

	        (yyval.entry) = actual;

            ;}
    break;

  case 33:
#line 951 "SintacticAnalizer.y"
    {
		(yyval.entry) = (yyvsp[(1) - (1)].entry);
            ;}
    break;

  case 34:
#line 956 "SintacticAnalizer.y"
    {
                if(searchVar((yyvsp[(1) - (1)].string),theTable_p)==NULL){
		    yyerror("xxxxxxxxx\nThe variable was not declared\nxxxxxxxxx");
		    yyerrorL(linea);
                    //printf ("Error the variable %s was not declared\n",$1);
                }else{
		   (yyval.entry) = searchVar((yyvsp[(1) - (1)].string),theTable_p);
		}
            ;}
    break;


/* Line 1267 of yacc.c.  */
#line 2413 "SintacticAnalizer.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 970 "SintacticAnalizer.y"


/**********************
 * Codigo C Adicional *
 **********************/

/* This is where the flex is included */
#include "lex.yy.c"

// we declare the yy functions
void yyerror (string input){
  printf ("%s\n",input);
  semantic_error=1;
}

void yyerrorL(int line){
  printf ("In the line:%d\n",line);
  printf("\n");
}

int main(int argc,char **argv)
{
    //Creas la tabla de Hash 
    theTable_p = g_hash_table_new(g_str_hash, g_str_equal);

    //Initialize the garray 
    garray = g_ptr_array_new();

    //Verifica que sea un argumento
    if (argc>1)
    yyin=fopen(argv[1],"rt");
    else
    //yyin=stdin;
    //Abre de forma de lectura el archivo que se va a compilar
    yyin=fopen("input.txt","rt");
    
    //Corres el codigo
    yyparse();
	int length;

    // Print al the quads that are generated
	
    // Print the entire  Hash table
    if(semantic_error==0){
    	printf("\n=================\n");
    	printf("NO SEMANTIC ERRORS\n");
    	printf("=================\n");
    }
    printf("\n\tPrint the entire table\n");
    /*
    if (PrintTable(theTable_p) != EXIT_SUCCESS)
    {
    	printf("Error while printing the symbol table");
    }*/
    
    printQuadTable(garray);

    //Now we are going to execute the quadruples
    theTable_p =execute_cycle(garray,theTable_p);
   
   printf("\n\tSecond Print the entire table\n");
    if (PrintTable(theTable_p) != EXIT_SUCCESS)
    {
    	printf("Error while printing the symbol table");
    }

    printf("Number of variables correctly declared: %d\n\n", number);

    //return DestroyTable(theTable_p);
}

